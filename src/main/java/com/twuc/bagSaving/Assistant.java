package com.twuc.bagSaving;

import java.util.ArrayList;
import java.util.List;

public class Assistant {
    protected List<Cabinet> cabinets = new ArrayList<>();

    public Assistant(Cabinet cabinet) {
        cabinets.add(cabinet);
    }

    public Assistant(List<Cabinet> cabinets) {
        this.cabinets = cabinets;
    }

    public Ticket saveBag(Bag bag) {
        LockerSize lockerSize = LockerSize.valueOf(bag.getBagSize().toString());
        for (Cabinet cabinet : cabinets) {
            try {
                return cabinet.save(bag, lockerSize);
            } catch (InsufficientLockersException | IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        throw new InsufficientLockersException("Insufficient empty lockers.");
    }

    public Bag getBag(Ticket ticket) {
        for (Cabinet cabinet : cabinets) {
            try {
                return cabinet.getBag(ticket);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        throw new IllegalArgumentException("Invalid ticket.");
    }
}
