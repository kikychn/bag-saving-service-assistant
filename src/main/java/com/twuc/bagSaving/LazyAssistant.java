package com.twuc.bagSaving;

import java.util.Arrays;
import java.util.List;

public class LazyAssistant extends Assistant{

    public LazyAssistant(Cabinet cabinet) {
        super(cabinet);
    }

    public LazyAssistant(List<Cabinet> cabinets) {
        super(cabinets);
    }

    @Override
    public Ticket saveBag(Bag bag) {
        LockerSize[] biggerLockSizes = Arrays.stream(LockerSize.values())
                .filter(lockerSize -> lockerSize.getSizeNumber() >= bag.getBagSize().getSizeNumber())
                .toArray(LockerSize[]::new);
        if (biggerLockSizes.length == 0) {
            throw new InsufficientLockersException("un-support bag size.");
        }

        for (Cabinet cabinet : cabinets) {
            for (LockerSize biggerLockerSize : biggerLockSizes) {
                try {
                    return cabinet.save(bag, biggerLockerSize);
                } catch (InsufficientLockersException | IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        throw new InsufficientLockersException("Insufficient empty lockers.");
    }

}
