package com.twuc.bagSaving;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

public class SkillfullAssistant extends Assistant {

    public SkillfullAssistant(List<Cabinet> cabinets) {
        super(cabinets);
    }

    @Override
    public Ticket saveBag(Bag bag) {
        LockerSize[] biggerLockerSizes = Arrays.stream(LockerSize.values())
                .filter(lockerSize -> lockerSize.getSizeNumber() >= bag.getBagSize().getSizeNumber())
                .toArray(LockerSize[]::new);
        List<LockerSize> biggerLockerSizeASC = asList(biggerLockerSizes);
        Collections.reverse(biggerLockerSizeASC);

        for (LockerSize biggerLockerSize : biggerLockerSizeASC) {
            for (Cabinet cabinet : cabinets) {
                try {
                    return cabinet.save(bag, biggerLockerSize);
                } catch (InsufficientLockersException | IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        throw new InsufficientLockersException("Insufficient empty lockers.");
    }

    public List<Cabinet> getCabinets() {
        return cabinets;
    }
}
