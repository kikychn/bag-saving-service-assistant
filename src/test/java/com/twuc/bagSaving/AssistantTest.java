package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class AssistantTest {
    @Test
    void should_save_smaller_bag_to_smaller_locker_using_stupid_assistant() {
        LockerSetting[] setting = new LockerSetting[1];
        setting[0] = LockerSetting.of(LockerSize.SMALL, 1);
        Cabinet cabinet = new Cabinet(setting);

        Bag smallerBag = new Bag(BagSize.SMALL);
        Assistant stupidAssistant = new Assistant(cabinet);
        stupidAssistant.saveBag(smallerBag);

        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () -> cabinet.save(smallerBag, LockerSize.SMALL));
        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @Test
    void should_get_bag_to_using_stupid_assistant() {
        LockerSetting[] setting = new LockerSetting[1];
        setting[0] = LockerSetting.of(LockerSize.SMALL, 1);
        Cabinet cabinet = new Cabinet(setting);

        Bag smallerBag = new Bag(BagSize.SMALL);
        Assistant stupidAssistant = new Assistant(cabinet);
        Ticket ticket = stupidAssistant.saveBag(smallerBag);
        Bag savedBag = stupidAssistant.getBag(ticket);

        assertSame(smallerBag, savedBag);
    }

    @Test
    void should_save_and_get_a_bag_to_the_second_cabinet_using_stupidassistant() {
        LockerSetting[] setting = new LockerSetting[1];
        setting[0] = LockerSetting.of(LockerSize.SMALL, 1);
        Cabinet cabinet1 = new Cabinet(setting);
        Cabinet cabinet2 = new Cabinet(setting);
        List<Cabinet> cabinets = new ArrayList<>();
        cabinets.add(cabinet1);
        cabinets.add(cabinet2);

        Bag smallerBag1 = new Bag(BagSize.SMALL);
        Bag smallerBag2 = new Bag(BagSize.SMALL);
        Assistant stupidAssistant = new Assistant(cabinets);
        Ticket ticket1 = stupidAssistant.saveBag(smallerBag1);
        Ticket ticket2 = stupidAssistant.saveBag(smallerBag2);
        Bag savedBag1 = stupidAssistant.getBag(ticket1);
        Bag savedBag2 = stupidAssistant.getBag(ticket2);

        assertSame(smallerBag1, savedBag1);
        assertSame(smallerBag2, savedBag2);
    }

    @Test
    void should_save_and_get_a_bag_by_different_stupidassistant() {
        LockerSetting[] setting = new LockerSetting[1];
        setting[0] = LockerSetting.of(LockerSize.SMALL, 1);
        Cabinet cabinet = new Cabinet(setting);

        Bag smallerBag = new Bag(BagSize.SMALL);
        Assistant stupidAssistant1 = new Assistant(cabinet);
        Assistant stupidAssistant2 = new Assistant(cabinet);
        Ticket ticket = stupidAssistant1.saveBag(smallerBag);
        Bag savedBag = stupidAssistant2.getBag(ticket);

        assertSame(smallerBag, savedBag);
    }

    @Test
    void should_get_tikcet_when_save_bag_using_lazy_assistant_given_a_meduim_bag_and_a_big_lockers() {
        LockerSetting[] oneLockerSettings = {LockerSetting.of(LockerSize.MEDIUM, 1), LockerSetting.of(LockerSize.BIG, 1)};
        Cabinet cabinet = new Cabinet(oneLockerSettings);
        LockerSetting[] anotherLockerSettings = {LockerSetting.of(LockerSize.BIG, 1)};
        Cabinet anotherCabinet = new Cabinet(anotherLockerSettings);

        LazyAssistant lazyAssistant = new LazyAssistant(Arrays.asList(cabinet, anotherCabinet));
        lazyAssistant.saveBag(new Bag(BagSize.MEDIUM));
        lazyAssistant.saveBag(new Bag(BagSize.BIG));
        Bag bag = new Bag(BagSize.MEDIUM);
        Ticket ticket = lazyAssistant.saveBag(bag);
        Bag savedBag = lazyAssistant.getBag(ticket);

        assertSame(bag, savedBag);
    }

    @Test
    void should_save_three_smaller_bags_by_order_using_skillfulassistant() {
        Cabinet cabinet1 = new Cabinet(new LockerSetting[]{LockerSetting.of(LockerSize.BIG, 1)});
        Cabinet cabinet2 = new Cabinet(new LockerSetting[]{LockerSetting.of(LockerSize.SMALL, 1)});
        Cabinet cabinet3 = new Cabinet(new LockerSetting[]{LockerSetting.of(LockerSize.MEDIUM, 1)});
        List<Cabinet> cabinets = Arrays.asList(cabinet1, cabinet2, cabinet3);

        SkillfullAssistant skillfullAssistant = new SkillfullAssistant(cabinets);
        Bag bag1 = new Bag(BagSize.SMALL);
        Ticket ticket1 = skillfullAssistant.saveBag(bag1);
        assertSame(bag1, skillfullAssistant.getCabinets().get(1).getBag(ticket1));
        skillfullAssistant.saveBag(bag1);

        Bag bag2 = new Bag(BagSize.SMALL);
        Ticket ticket2 = skillfullAssistant.saveBag(bag2);
        assertSame(bag2,skillfullAssistant.getCabinets().get(2).getBag(ticket2));
        skillfullAssistant.saveBag(bag2);

        Bag bag3 = new Bag(BagSize.SMALL);
        Ticket ticket3 = skillfullAssistant.saveBag(bag3);
        assertSame(bag3,skillfullAssistant.getCabinets().get(0).getBag(ticket3));
        skillfullAssistant.saveBag(bag3);
    }
}
